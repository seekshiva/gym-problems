import gym
from gym import envs
from functools import reduce
env = gym.make('CartPole-v0')
# print(envs.registry.all())
# print('')
# print(env.observation_space)
# print(env.observation_space.high)
# print(env.observation_space.low)
total_reward = 0

starting_points = []

allowed_neg_actions = [
    list(range(env.action_space.n)),
    list(range(env.action_space.n)),
    list(range(env.action_space.n)),
    list(range(env.action_space.n)),
]
allowed_pos_actions = [
    list(range(env.action_space.n)),
    list(range(env.action_space.n)),
    list(range(env.action_space.n)),
    list(range(env.action_space.n)),
]

prev_action = '_'
action = '_'
for i_episode in range(1):
    total_reward = 0
    observation = env.reset()
    starting_point = observation
    starting_points.append(observation)
    sum = reduce(lambda x,y: [x[0]+y[0], x[1]+y[1], x[2]+y[2], x[3]+y[3]],starting_points)
    count = len(starting_points)
    average = list(map(lambda x: x/count, sum))
    diff = [
        starting_point[0]-observation[0],
        starting_point[1]-observation[1],
        starting_point[2]-observation[2],
        starting_point[3]-observation[3]
    ]
    prev_diff = diff
    # print(average)
    for t in range(1000):
        # env.render()
        print('')
        print('_ → {}'.format(observation))
        prev_diff = diff
        diff = [
            starting_point[0]-observation[0],
            starting_point[1]-observation[1],
            starting_point[2]-observation[2],
            starting_point[3]-observation[3]
        ]
        print('{} → {}'.format(prev_action, diff))
        arr = []
        for i in list(range(len(diff))):
            if (abs(prev_diff[i]) < abs(diff[i])):
                print('Remove {} from {} at position:{}'.format(prev_action, 'neg' if prev_diff[i] < 0 else 'pos', i))
                if prev_diff[i] < 0:
                    try:
                        allowed_neg_actions[i].remove(prev_action)
                    except:
                        pass
                if prev_diff[i] > 0:
                    try:
                        allowed_pos_actions[i].remove(prev_action)
                    except:
                        pass

        print('dd→ {}'.format(arr))
        arr = []
        for i in list(range(len(diff))):
            arr.append(allowed_neg_actions[i] if diff[i] < 0 else allowed_pos_actions[i])
            # print('{} {}'.format(i, )
        print(arr)
        print('Next action: {}'.format(action))
        prev_action = action
        action = env.action_space.sample()
        observation, reward, done, info = env.step(action)
        total_reward += reward
        if done:
            print('{} → {}'.format(action, observation))
            # print('Episode finished after {} timesteps. Reward {}'.format(t+1, total_reward))
            break
